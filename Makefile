GRPC_DIR=src/grpc/

RED:=$(shell tput -Txterm setaf 1)
RESET:= $(shell tput -Txterm sgr0)

run: build
	@echo "${RED}*** Running ***${RESET}"
	@NODE_ADDRESS=localhost:8080 ./bin/node --console

run_2: build
	@echo "${RED}*** Running ***${RESET}"
	@NODE_ADDRESS=localhost:8081 ./bin/node --console

build:
	@echo "${RED}*** Building ***${RESET}"
	@go build -o bin/node src/cmd/node/main.go

docker: build
	@echo "${RED}*** Docker-compose up ***${RESET}"
	@docker-compose -f docker/docker-compose.yml up

attach:
	@echo "${RED}*** Attaching to node container ***${RESET}"
	@docker attach node

stop:
	@echo "${RED}*** Docker-compose stop ***${RESET}"
	@docker-compose -f docker/docker-compose.yml stop

clean:
	@rm -rf bin
	@rm -rf src/grpc/pb