package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/adaptares/left/console"
	httpserver "gitlab.com/stickman_0x00/another_crypto/src/internal/adaptares/left/http"
	httpclient "gitlab.com/stickman_0x00/another_crypto/src/internal/adaptares/right/http"
	tempdb "gitlab.com/stickman_0x00/another_crypto/src/internal/adaptares/right/tempDB"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/api"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/blockchain"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/peers"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/ports"
)

var use_console = flag.Bool("console", false, "Use console")

var BLOCKCHAIN core.Blockchain
var PEERS core.Peers
var CLIENT ports.Client
var API ports.API
var DB ports.DB

func main() {
	flag.Parse()

	address := os.Getenv("NODE_ADDRESS")
	if address == "" {
		address = ":8080"
	}

	BLOCKCHAIN = blockchain.New()
	PEERS = peers.New()

	CLIENT = httpclient.New(address)
	DB = tempdb.New()

	API = api.New(BLOCKCHAIN, PEERS, DB, CLIENT)

	SERVER := httpserver.New(API)
	CONSOLE := console.New(API, SERVER)

	var w sync.WaitGroup

	if *use_console {
		w.Add(1)
		go func() {
			CONSOLE.Run()

			w.Done()
		}()
	}

	w.Add(1)
	go func() {
		if err := SERVER.Start(address); err != nil && err != http.ErrServerClosed {
			log.Println(err)
		}

		w.Done()
	}()

	// for i := 0; i < 31; i++ {
	// 	_, err := API.Mine_block()
	// 	if err != nil {
	// 		log.Println(err)
	// 	}
	// }

	time.Sleep(1 * time.Second)

	w.Wait()
}
