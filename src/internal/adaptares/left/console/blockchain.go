package console

import (
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

func (me Console) mine_block(_ []string) any {
	b, err := me.api.Mine_block()
	if err != nil {
		return err
	}

	return b
}

func (me Console) blocks(_ []string) any {
	blocks := []core.Block{}
	for i := -1; i >= -10; i-- {
		b := me.api.Block(i)
		if b == nil {
			break
		}

		blocks = append(blocks, b)
	}

	return blocks
}
