package console

type command_func func(args []string) any

type command struct {
	commands commands
	f        command_func
	n_args   int
}

type commands map[string]*command

func new_command(n_args int, f command_func) *command {
	return &command{
		commands: commands{},
		f:        f,
		n_args:   n_args,
	}
}

func (me *Console) set_commands() {
	me.commands.commands["blockchain"] = new_command(1, nil)
	me.commands.commands["blockchain"].commands["mine"] = new_command(0, me.mine_block)
	me.commands.commands["blockchain"].commands["blocks"] = new_command(0, me.blocks)
	me.commands.commands["mine"] = new_command(0, me.mine_block)

	me.commands.commands["peers"] = new_command(0, me.peers)
	me.commands.commands["peers"].commands["new"] = new_command(1, me.new_peer)
}
