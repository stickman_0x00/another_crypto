package console

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/ports"
)

type Console struct {
	reader   *bufio.Reader
	api      ports.API
	services []Service
	commands *command
}

func New(api ports.API, services ...Service) *Console {
	me := &Console{
		reader:   bufio.NewReader(os.Stdin),
		api:      api,
		services: services,
		commands: new_command(1, nil),
	}

	me.set_commands()

	return me
}

func (me Console) Run() {
	for {
		command := me.read_text()

		if command == "stop" {
			me.stop()
			return
		}

		commands_parts := strings.Split(command, " ")

		output := me.execute(me.commands, commands_parts)
		if output == nil {
			continue
		}

		rt := reflect.TypeOf(output)
		switch rt.Kind() {
		case reflect.Slice, reflect.Array:
			v := reflect.ValueOf(output)

			for i := 0; i < v.Len(); i++ {
				fmt.Println(v.Index(i).Interface())
			}
		default:
			fmt.Println(output)
		}
	}
}

func (me Console) execute(c *command, command_parts []string) any {
	if c.f != nil {
		if c.n_args == len(command_parts) {
			return c.f(command_parts)
		}
	}

	if len(command_parts) > 0 {
		if c, exist := c.commands[command_parts[0]]; exist {
			return me.execute(c, command_parts[1:])
		}
	}

	return errors.New("invalid command")
}
