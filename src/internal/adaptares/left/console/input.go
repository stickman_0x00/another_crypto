package console

import "fmt"

func (me Console) read_text() string {
	for {
		fmt.Print("───➤ ")

		text, err := me.reader.ReadString('\n')
		if err != nil || text == "\n" {
			continue
		}

		text = text[:len(text)-1]

		return text
	}
}
