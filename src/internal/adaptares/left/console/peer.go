package console

import "errors"

func (me Console) new_peer(args []string) any {
	if len(args) != 1 {
		return errors.New("invalid number of arguments")
	}

	err := me.api.Add_peer(args[0])
	if err != nil {
		return err
	}

	return nil
}

func (me Console) peers(_ []string) any {
	return me.api.Peers()
}
