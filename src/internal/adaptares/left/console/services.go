package console

import "log"

type Service interface {
	Stop() error
}

func (me Console) stop() {
	var err error
	for _, s := range me.services {
		err = s.Stop()
		if err != nil {
			log.Println("error stopping service: ", err)
		}
	}
}
