package httpserver

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/api"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/block"
)

// GET /block/{index}
func (me HTTP_Server) block(response http.ResponseWriter, request *http.Request) {
	index, err := strconv.Atoi(request.PathValue("index"))
	if err != nil {
		http.Error(response, "invalid index", http.StatusBadRequest)
		return
	}

	block := me.api.Block(index)
	if block == nil {
		http.Error(response, "Block not found", http.StatusNotFound)
		return
	}

	send(response, http.StatusOK, block)
}

// POST /block
func (me HTTP_Server) add_block(response http.ResponseWriter, request *http.Request) {
	var block block.Block

	err := json.NewDecoder(request.Body).Decode(&block)
	if err != nil {
		http.Error(response, "invalid block", http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	err = me.api.Add_block(&block)
	if err != nil && err != api.ERR_BLOCK_EXISTS {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	send(response, http.StatusCreated, nil)
}
