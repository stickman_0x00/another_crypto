package httpserver

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/peers"
	log "gitlab.com/stickman_0x00/go_log"
)

// Connected receives alerts from peers that they are connected
// and adds them to the list of peers.
func (me HTTP_Server) connected(response http.ResponseWriter, request *http.Request) {
	peer := &peers.Peer{}

	err := json.NewDecoder(request.Body).Decode(peer)
	if err != nil {
		log.Debugf("Failed decoding peer: %s\n", err)
		http.Error(response, "invalid peer", http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	err = me.api.Add_peer(peer.Address())
	if err != nil {
		log.Debugf("Failed adding peer: %s\n", err)
		http.Error(response, "Failed adding peer", http.StatusInternalServerError)
		return
	}

	send(response, http.StatusCreated, nil)
}

func (me HTTP_Server) peers(response http.ResponseWriter, request *http.Request) {
	send(response, http.StatusOK, me.api.Peers())
}
