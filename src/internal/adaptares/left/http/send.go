package httpserver

import (
	"encoding/json"
	"net/http"
)

func send(response http.ResponseWriter, status int, data any) {
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(status)

	err := json.NewEncoder(response).Encode(data)
	if err != nil {
		http.Error(response, "Internal server error", http.StatusInternalServerError)
		return
	}
}
