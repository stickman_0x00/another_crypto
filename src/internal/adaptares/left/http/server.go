package httpserver

import (
	"net/http"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/ports"
)

type HTTP_Server struct {
	api ports.API

	server *http.ServeMux
}

func New(api ports.API) *HTTP_Server {
	return &HTTP_Server{
		api:    api,
		server: http.NewServeMux(),
	}
}

func (me HTTP_Server) Start(address string) error {
	me.server.HandleFunc("GET /block/{index}", me.block)
	me.server.HandleFunc("POST /block", me.add_block)

	me.server.HandleFunc("GET /peers", me.peers)
	me.server.HandleFunc("POST /peer", me.connected)

	return http.ListenAndServe(address, me.server)
}

func (me HTTP_Server) Stop() error {
	return nil
}
