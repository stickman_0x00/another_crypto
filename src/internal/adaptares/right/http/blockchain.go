package httpclient

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/block"
)

// GET /block/{index}
func (me *HTTP_Client) Block(peer core.Peer, index int) (core.Block, error) {
	path := fmt.Sprintf("/block/%d", index)
	response, err := me.do(peer, http.MethodGet, path, nil)
	if err != nil {
		return nil, err
	}

	bl := &block.Block{}

	err = json.Unmarshal(response, &bl)
	if err != nil {
		return nil, err
	}

	return bl, nil
}

// POST /block
func (me *HTTP_Client) Add_block(peer core.Peer, bl core.Block) error {
	_, err := me.do(peer, http.MethodPost, "/block", bl)
	if err != nil {
		return err
	}

	return nil
}
