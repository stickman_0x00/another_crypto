package httpclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

type HTTP_Client struct {
	address  string
	protocol string
}

// New creates a new HTTP_Client.
// The address is the address of the server.
func New(address string) *HTTP_Client {
	return &HTTP_Client{
		address:  address,
		protocol: "http://",
	}
}

func (me HTTP_Client) do(peer core.Peer, method string, path string, body any) ([]byte, error) {
	json_body, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	buffer_body := bytes.NewBuffer(json_body)

	request, err := http.NewRequest(method, me.protocol+peer.Address()+path, buffer_body)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	response_body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusCreated {
		return nil, errors.New("bad status code: " + response.Status + " error: " + string(response_body))
	}

	return response_body, nil
}
