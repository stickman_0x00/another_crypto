package httpclient

import (
	"net/http"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

func (me *HTTP_Client) Connected(peer core.Peer) error {
	body := struct {
		Address string
	}{
		Address: me.address,
	}

	_, err := me.do(peer, http.MethodPost, "/peer", body)
	if err != nil {
		return err
	}

	return nil
}
