package tempdb

import (
	"errors"
	"slices"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

type TempDB struct {
	blocks []core.Block
	length int
}

func New() *TempDB {
	return &TempDB{
		blocks: []core.Block{},
		length: 0,
	}
}

func (me *TempDB) Add(block core.Block) error {
	if me.Block(block.Index()) != nil {
		return errors.New("block already exists")
	}

	me.blocks = append(me.blocks, block)
	me.length++

	return nil
}

func (me *TempDB) Blocks(start, end int) []core.Block {
	start_block := me.Block(start)
	if start_block == nil {
		if me.length+start < 0 {
			start = 0
		}
	} else {
		start = start_block.Index()
	}

	end_block := me.Block(end)
	if end_block == nil {
		if me.length+end < 0 {
			end = 0
		}
	} else {
		end = end_block.Index()
	}

	blocks := []core.Block{}

	for i := start; i <= end; i++ {
		block := me.Block(i)
		if block == nil {
			break
		}

		blocks = append(blocks, block)
	}

	return blocks
}

func (me *TempDB) Block(index int) core.Block {
	if index < 0 {
		// Conversion of index
		index = me.length + index
	}

	// Check if index is valid.
	if index < 0 || index >= me.length {
		return nil
	}

	return me.blocks[index]
}

func (me *TempDB) Length() int {
	return me.length
}

func (me *TempDB) Delete() error {
	if me.length == 0 {
		return errors.New("no blocks to delete")
	}

	me.blocks = slices.Delete(me.blocks, len(me.blocks)-1, len(me.blocks))

	return nil
}
