package api

import (
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/ports"
)

type API struct {
	blockchain core.Blockchain
	peers      core.Peers

	db     ports.DB
	client ports.Client

	events chan Event
}

func New(blockchain core.Blockchain, peers core.Peers, db ports.DB, client ports.Client) ports.API {
	api := &API{
		blockchain: blockchain,
		peers:      peers,
		db:         db,
		client:     client,
		events:     make(chan Event, 2),
	}

	api.Mine_block()
	go api.handle_events()

	return api
}
