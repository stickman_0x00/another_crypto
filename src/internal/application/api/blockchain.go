package api

import (
	"bytes"
	"errors"
	"time"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/block"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/blockchain"
	log "gitlab.com/stickman_0x00/go_log"
)

//lint:ignore ST1012 -
var ERR_BLOCK_EXISTS = errors.New("Block already exists")

func (me API) Block(index int) core.Block {
	return me.db.Block(index)
}

func (me API) Add_block(block core.Block) error {
	if bl := me.db.Block(block.Index()); bl != nil {
		if bytes.Equal(bl.Hash(), block.Hash()) {
			return ERR_BLOCK_EXISTS
		}

		return errors.New("Block already exists")
	}

	err := me.blockchain.Is_valid_next_block(block, me.Block(-1))
	if err != nil {
		return err
	}

	me.events <- Event{Type: EVENT_BROADCAST_BLOCK, Data: block}

	return me.db.Add(block)
}

func (me API) Mine_block() (core.Block, error) {
	last_block := me.Block(-1)

	if last_block == nil {
		// This is the first block in the chain.
		genesis_block := me.blockchain.Genesis()
		err := me.db.Add(genesis_block)
		if err != nil {
			return nil, err
		}

		return genesis_block, nil
	}

	difficulty := me.blockchain.Consensus(me.db.Blocks(-blockchain.DIFFICULTY_ADJUSTMENT_INTERVAL, -1))

	log.Infoln("Mining block with difficulty:", difficulty)

	new_block := block.New(
		last_block.Index()+1,
		difficulty,
		0,
		last_block.Hash(),
		time.Now(),
	)

	me.blockchain.PoW(new_block)

	err := me.Add_block(new_block)
	if err != nil {
		return nil, err
	}

	return new_block, nil
}
