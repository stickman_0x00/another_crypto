package api

import (
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	log "gitlab.com/stickman_0x00/go_log"
)

type event int

func (me event) String() string {
	return [...]string{
		"Broadcast block",

		"Connecting to peer",
		"Synchronize with peer",
	}[me]
}

const (
	EVENT_BROADCAST_BLOCK event = iota

	EVENT_CONNECT_PEER
	EVENT_SYNC_PEER
)

// Event struct
type Event struct {
	Type event
	Data any
}

// handle_events handles events from the event channel.
func (me *API) handle_events() {
	for event := range me.events {
		// log.Println("Event:", event.Type, event.Data)

		switch event.Type {
		case EVENT_BROADCAST_BLOCK:
			block := event.Data.(core.Block)

			me.broadcast_block(block)

		case EVENT_CONNECT_PEER:
			peer := event.Data.(core.Peer)

			me.connect_peer(peer)
		case EVENT_SYNC_PEER:
			peer := event.Data.(core.Peer)

			err := me.sync_peer(peer)
			if err != nil {
				log.Errorf("Failed synchronizing with peer: %s: %s\n", peer.Address(), err)
			}

		}

	}
}
