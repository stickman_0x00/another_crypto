package api

import (
	"bytes"
	"errors"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	log "gitlab.com/stickman_0x00/go_log"
)

//lint:ignore ST1012 -
var ERR_DIFFERENT_CHAIN = errors.New("peer is on a different chain")

func (me API) broadcast_block(block core.Block) {
	log.Info("Broadcasting block: ", block.Index())

	connected_peers := me.peers.Peers()
	for _, peer := range connected_peers {
		err := me.client.Add_block(peer, block)
		if err != nil {
			log.Infof("Failed broadcasting block to peer: %s: %s\n", peer.Address(), err)
		}
	}
}

func (me API) Add_peer(address string) error {
	peer := me.peers.Add(address)

	if peer.Connected() {
		return nil
	}

	log.Info("Peer added:", peer.Address())

	// Connect to peer.
	me.events <- Event{Type: EVENT_CONNECT_PEER, Data: peer}

	return nil
}

func (me API) Peers() []core.Peer {
	return me.peers.Peers()
}

func (me *API) connect_peer(peer core.Peer) {
	log.Info("Connecting to peer:", peer.Address())

	err := me.validate_peer(peer)
	if err != nil {
		me.peers.Remove(peer.Address())
		log.Infof("Invalid peer removed %s: %s\n", peer.Address(), err)
		return
	}

	peer.Set_connected(true)
	log.Info("Connected to peer: ", peer.Address())

	// Alert connection established.
	err = me.client.Connected(peer)
	if err != nil {
		log.Infof("Failed alerting peer connection established: %s: %s\n", peer.Address(), err)
		return
	}
}

func (me *API) validate_peer(peer core.Peer) error {
	log.Info("Validating peer: ", peer.Address())

	// Get peer last block.
	peer_lb, err := me.client.Block(peer, -1)
	if err != nil {
		return err
	}

	last_block := me.Block(-1)
	// Check if same block.
	if last_block.Index() == peer_lb.Index() {
		// Check if peer is on a different chain.
		if !bytes.Equal(last_block.Hash(), peer_lb.Hash()) {
			return ERR_DIFFERENT_CHAIN
		}

		return nil // Peer is on the same block.
	}

	// Check if peer is behind.
	if last_block.Index() > peer_lb.Index() {
		// Check if peer is on a different chain.
		if !bytes.Equal(me.Block(peer_lb.Index()).Hash(), peer_lb.Hash()) {
			return ERR_DIFFERENT_CHAIN
		}

		return nil // Peer is behind.
	}

	// Peer is ahead.

	// Get peer our current block.
	peer_block, err := me.client.Block(peer, last_block.Index())
	if err != nil {
		return err
	}

	// Check if peer is on a different chain.
	if !bytes.Equal(last_block.Hash(), peer_block.Hash()) {
		return ERR_DIFFERENT_CHAIN
	}

	// Sync.

	me.events <- Event{Type: EVENT_SYNC_PEER, Data: peer}

	return nil
}

func (me *API) sync_peer(peer core.Peer) error {
	log.Info("Synchronizing:", peer.Address())

	// Get peer last block.
	peer_lb, err := me.client.Block(peer, -1)
	if err != nil {
		return err
	}

	last_block := me.Block(-1)

	for i := last_block.Index() + 1; i <= peer_lb.Index(); i++ {
		log.Infof("Synchronizing block %d\n", i)
		// Get next block from peer.
		received_block, err := me.client.Block(peer, i)
		if err != nil {
			return err
		}

		// Add block to blockchain.
		err = me.Add_block(received_block)
		if err != nil {
			return err
		}
	}

	log.Info("Synchronization completed:", peer.Address())

	return nil
}
