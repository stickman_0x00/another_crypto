package block

import (
	"time"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

type Block struct {
	index      int
	difficulty int
	nonce      int
	hash       []byte
	prev_block []byte
	time       time.Time
}

func New(index int, difficulty int, nonce int, prev_block []byte, t time.Time) core.Block {
	b := &Block{
		index:      index,
		difficulty: difficulty,
		nonce:      nonce,
		prev_block: prev_block,
		time:       t,
	}

	b.calculate_hash()

	return b
}

func (me Block) Index() int {
	return me.index
}

func (me Block) Difficulty() int {
	return me.difficulty
}

func (me Block) Nonce() int {
	return me.nonce
}

func (me *Block) Inc_nonce() {
	me.nonce++
	me.calculate_hash()
}

func (me Block) Hash() []byte {
	return me.hash
}

func (me Block) Prev_block() []byte {
	return me.prev_block
}

func (me Block) Time() time.Time {
	return me.time
}
