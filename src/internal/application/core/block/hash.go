package block

import (
	"bytes"
	"crypto/sha512"
	"encoding/binary"
)

func (me *Block) calculate_hash() {
	buffer := new(bytes.Buffer)

	binary.Write(buffer, binary.BigEndian, int64(me.index))

	binary.Write(buffer, binary.BigEndian, int64(me.difficulty))

	binary.Write(buffer, binary.BigEndian, int64(me.nonce))

	buffer.Write(me.prev_block)

	binary.Write(buffer, binary.BigEndian, me.time.UnixMilli())

	h := sha512.Sum512(buffer.Bytes())
	me.hash = h[:]
}
