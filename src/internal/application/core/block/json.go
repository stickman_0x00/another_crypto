package block

import (
	"encoding/hex"
	"encoding/json"
	"time"
)

type json_block struct {
	Index      int    `json:"index"`
	Difficulty int    `json:"difficulty"`
	Nonce      int    `json:"nonce"`
	Hash       string `json:"hash"`
	Prev_block string `json:"prev_block"`
	Time       int64  `json:"time"`
}

func (b Block) MarshalJSON() ([]byte, error) {
	return json.Marshal(&json_block{
		Index:      b.index,
		Difficulty: b.difficulty,
		Nonce:      b.nonce,
		Hash:       hex.EncodeToString(b.hash),
		Prev_block: hex.EncodeToString(b.prev_block),
		Time:       b.time.Unix(),
	})
}

func (me *Block) UnmarshalJSON(data []byte) error {
	var b json_block
	err := json.Unmarshal(data, &b)
	if err != nil {
		return err
	}

	me.index = b.Index

	me.hash, err = hex.DecodeString(b.Hash)
	if err != nil {
		return err
	}
	me.prev_block, err = hex.DecodeString(b.Prev_block)
	if err != nil {
		return err
	}

	me.difficulty = b.Difficulty
	me.nonce = b.Nonce
	me.time = time.Unix(b.Time, 0)

	return nil
}
