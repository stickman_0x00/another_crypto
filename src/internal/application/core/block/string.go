package block

import (
	"fmt"
	"time"
)

var blockf = "Index:\t\t%d" +
	"\nDifficulty:\t%d" +
	"\nNonce:\t\t%d" +
	"\nHash:\t\t%s" +
	"\nPrev Block:\t%s" +
	"\nTime:\t\t%s"

func (me Block) String() string {
	a := fmt.Sprintf(
		blockf,
		me.index,
		me.difficulty,
		me.nonce,
		short_hash(me.hash),
		short_hash(me.prev_block),
		me.time.Format(time.RFC1123Z),
	)
	return a
}

func short_hash(h []byte) string {
	len_h := len(h)
	if len_h == 0 {
		return ""
	}

	return fmt.Sprintf("0x%x...%x", h[:6], h[len_h-6:])
}
