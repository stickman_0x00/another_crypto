package blockchain

import (
	"bytes"
	"errors"
	"fmt"
	"time"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

type Blockchain struct {
}

func New() core.Blockchain {
	return &Blockchain{}
}

func (me *Blockchain) Is_valid_next_block(new_block core.Block, last_block core.Block) error {
	// 1 - Verify the index of the new block.
	if new_block.Index() != last_block.Index()+1 {
		return fmt.Errorf("invalid index: %d", new_block.Index())
	}

	// 2 - Verify the previous hash of the new block.
	if !bytes.Equal(new_block.Prev_block(), last_block.Hash()) {
		return errors.New("invalid previous hash")
	}

	// 3 - Verify the timestamp of the new block.
	// A block is valid if the timestamp is at most 1 min in the future from the time we perceive.
	// In other words, if the timestamp of a block indicates a time that is more than 1 minute ahead of
	// the current system time, the block is considered invalid.
	// This condition ensures that a block's timestamp is not unrealistically ahead of the current system
	// time. It prevents blocks from being added to the blockchain with timestamps that are too far into
	// the future, which could be an attempt to manipulate the difficulty calculation.
	if new_block.Time().After(time.Now().Add(1 * time.Minute)) {
		return errors.New("invalid block time, block is in the future")
	}

	// A block in the chain is valid if the timestamp is at most 1 min in the past of the previous block.
	// This condition ensures that a block's timestamp is not unrealistically behind the timestamp of the
	// previous block in the chain. It prevents blocks from being added to the blockchain with timestamps that
	// are too far into the past compared to the previous block. This helps maintain the chronological order of
	/// blocks in the blockchain.
	if new_block.Time().Before(last_block.Time().Add(-1 * time.Minute)) {
		return errors.New("invalid block time, block is too old")
	}

	// 4 - Verify the difficulty of the new block.
	if !verify_difficulty(new_block) {
		return errors.New("invalid block hash")
	}

	return nil
}
