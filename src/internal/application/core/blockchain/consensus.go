package blockchain

import "gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"

const (
	// defines how often a block should be found
	BLOCK_GENERATION_INTERVAL = 10 // in seconds
	// defines how often the difficulty should adjust to the increasing or decreasing network hashrate
	DIFFICULTY_ADJUSTMENT_INTERVAL = 10 // in blocks
	//The expected time represents the case where the hashrate matches exactly the current difficulty.
	TIME_EXPECTED = BLOCK_GENERATION_INTERVAL * DIFFICULTY_ADJUSTMENT_INTERVAL
)

func (me Blockchain) Consensus(bc []core.Block) int {
	// For every 10 blocks that is generated, we check if the time that took to generate
	// those blocks are larger or smaller than the expected time.
	// If the index of the last block is a multiple of the DIFFICULTY_ADJUSTMENT_INTERVAL
	// and != 0.
	index := bc[len(bc)-1].Index()
	if index%DIFFICULTY_ADJUSTMENT_INTERVAL == 0 && index != 0 {
		return adjust_difficulty(bc)
	}

	return bc[len(bc)-1].Difficulty()
}

func adjust_difficulty(bc []core.Block) int {
	index_prev_adjustment_block := len(bc) - DIFFICULTY_ADJUSTMENT_INTERVAL
	prev_adjustment_block := bc[index_prev_adjustment_block]
	time_taken := bc[len(bc)-1].Time().Unix() - prev_adjustment_block.Time().Unix()

	if time_taken < TIME_EXPECTED/2 { // less than half the expected block creation time
		return prev_adjustment_block.Difficulty() + 1
	} else if time_taken > TIME_EXPECTED*2 { // more than twice the expected block creation time
		return prev_adjustment_block.Difficulty() - 1
	}

	// Stays the same difficulty
	return prev_adjustment_block.Difficulty()

}
