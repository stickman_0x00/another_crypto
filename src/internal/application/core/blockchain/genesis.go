package blockchain

import (
	"time"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core/block"
)

func (me *Blockchain) Genesis() core.Block {
	return block.New(0, 0, 0, []byte{}, time.Unix(0, 0))
}
