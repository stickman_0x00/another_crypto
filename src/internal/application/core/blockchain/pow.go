package blockchain

import (
	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

func (me Blockchain) PoW(b core.Block) {
	for {
		if verify_difficulty(b) {
			return
		}

		b.Inc_nonce()
	}
}

// verify_difficulty returns true if the hash of the block is valid
// for the difficulty of the block.
func verify_difficulty(b core.Block) bool {
	difficulty := b.Difficulty()
	h := b.Hash()

	for i := 0; i < difficulty; i++ {
		if h[i] != 0 {
			return false
		}
	}

	return true
}
