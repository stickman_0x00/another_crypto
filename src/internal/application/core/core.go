package core

import (
	"time"
)

type Block interface {
	// Index returns the index of the block.
	Index() int
	// Difficulty returns the difficulty of the block.
	Difficulty() int
	// Nonce returns the nonce of the block.
	Nonce() int
	// Inc_nonce increments the nonce of the block,
	// and recalculates the hash.
	Inc_nonce()
	// Hash returns the hash of the block.
	Hash() []byte
	// Prev_block returns the previous hash of the previous block.
	Prev_block() []byte
	// Time returns the time of the block.
	Time() time.Time
}

type Blockchain interface {
	// Genesis creates the genesis block.
	Genesis() Block

	// PoW is a function that will increment the nonce of a block until the hash of
	// the block is valid.
	PoW(Block)

	// Consensus is a function that will return the difficulty of the next block.
	// [last_block-DIFFICULTY_ADJUSTMENT_INTERVAL..last_block].
	Consensus([]Block) int

	// Is_valid_next_block is a function that will return an error if the new block
	// is not valid.
	Is_valid_next_block(new_block Block, last_block Block) error
}

type Peer interface {
	// Address returns the address of the peer.
	Address() string
	// Connected returns true if the peer is connected, false otherwise.
	Connected() bool
	// Set_connected sets the connection status of the peer.
	Set_connected(bool)
}

type Peers interface {
	// Add adds a new peer to the peers.
	// If the peer already exists, it is returned.
	Add(string) Peer
	// Remove removes the peer with the given address.
	Remove(string)

	// Peer returns the peer with the given address.
	// If the peer does not exist, nil is returned.
	Peer(string) Peer
	//  Peers returns the connected peers.
	Peers() []Peer
}
