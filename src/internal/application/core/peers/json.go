package peers

import (
	"encoding/json"
)

type json_peer struct {
	Address string `json:"address"`
}

func (me Peer) MarshalJSON() ([]byte, error) {
	return json.Marshal(&json_peer{
		Address: me.address,
	})
}

func (me *Peer) UnmarshalJSON(data []byte) error {
	var p json_peer
	err := json.Unmarshal(data, &p)
	if err != nil {
		return err
	}

	me.address = p.Address

	return nil
}
