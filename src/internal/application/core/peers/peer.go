package peers

type Peer struct {
	address   string
	connected bool
}

func (me Peer) Address() string {
	return me.address
}

func (me Peer) Connected() bool {
	return me.connected
}

func (me *Peer) Set_connected(connected bool) {
	me.connected = connected
}
