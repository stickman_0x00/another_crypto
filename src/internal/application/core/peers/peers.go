package peers

import (
	"slices"

	"gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"
)

type Peers struct {
	peers []core.Peer
}

func New() *Peers {
	return &Peers{
		peers: []core.Peer{},
	}
}

func (me *Peers) Add(address string) core.Peer {
	// Check if the peer already exists.
	peer := me.Peer(address)
	if peer != nil {
		// If it does, return it.
		return peer
	}

	// If it doesn't, create it.
	peer = &Peer{
		address:   address,
		connected: false,
	}

	me.peers = append(me.peers, peer)

	return peer
}

func (me *Peers) Remove(address string) {
	for i, peer := range me.peers {
		if peer.Address() == address {
			me.peers = slices.Delete(me.peers, i, i+1)
			return
		}
	}
}

func (me Peers) Peer(address string) core.Peer {
	for _, peer := range me.peers {
		if peer.Address() == address {
			// Peer found.
			return peer
		}
	}

	// Peer not found.
	return nil
}

func (me Peers) Peers() []core.Peer {
	peers := []core.Peer{}

	for _, peer := range me.peers {
		if peer.Connected() {
			peers = append(peers, peer)
		}
	}

	return peers
}
