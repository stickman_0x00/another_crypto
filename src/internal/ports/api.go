package ports

import "gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"

type API interface {
	// Block returns the block at the given index.
	Block(index int) core.Block

	// Add_block adds a block to the blockchain.
	Add_block(core.Block) error

	// Mine_block mines a new block and adds it to the blockchain.
	Mine_block() (core.Block, error)

	// Peers returns the peers of the node
	Peers() []core.Peer

	// Add_peer adds a peer to the node
	Add_peer(address string) error
}
