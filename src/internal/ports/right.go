package ports

import "gitlab.com/stickman_0x00/another_crypto/src/internal/application/core"

type Client interface {
	// Add_block adds a block to the blockchain of the given peer.
	Add_block(peer core.Peer, block core.Block) error

	// Block returns the block of the given peer.
	Block(peer core.Peer, index int) (core.Block, error)

	// Connected alerts the client that the peer is connected.
	Connected(peer core.Peer) error
}

type DB interface {
	// Add adds a block to the database.
	Add(block core.Block) error

	// Blocks returns a range of blocks from the database.
	Blocks(start int, end int) []core.Block

	// Block returns the block at the given index.
	// If index is negative, it returns the block at the given index from the end.
	Block(index int) core.Block

	// Delete deletes the last block from the database.
	Delete() error

	// Length returns the length of the database.
	Length() int
}
